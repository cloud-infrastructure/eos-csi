package eos

import (
	"io/ioutil"
	"os"
	"path"
	"text/template"
)

const (
	eosConfigRoot = "/etc/eos"
)

const repoConf = `
`

var (
	repoConfTempl *template.Template
)

func init() {
	fs := map[string]interface{}{
		"fileContents": func(filePath string) string {
			if c, err := ioutil.ReadFile(filePath); err != nil {
				panic(err)
			} else {
				return string(c)
			}
		},
		"cacheBase": getVolumeCachePath,
	}

	repoConfTempl = template.Must(template.New("repo-conf").Funcs(fs).Parse(repoConf))
}

type eosConfigData struct {
}

func (d *eosConfigData) writeToFile() error {
	f, err := os.OpenFile(getConfigFilePath(d.VolUuid), os.O_CREATE|os.O_EXCL|os.O_WRONLY, 0755)
	if err != nil {
		if os.IsExist(err) {
			return nil
		}
		return err
	}

	defer f.Close()

	return repoConfTempl.Execute(f, d)
}

func getConfigFilePath(volUuid string) string {
	return path.Join(eosConfigRoot, "config-csi-"+volUuid)
}
