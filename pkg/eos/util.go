package eos

import (
	"fmt"
	"os/exec"
)

func execCommand(program string, args ...string) ([]byte, error) {
	cmd := exec.Command(program, args[:]...)
	return cmd.CombinedOutput()
}

func execCommandAndValidate(program string, args ...string) error {
	if out, err := execCommand(program, args[:]...); err != nil {
		return fmt.Errorf("eos: %s failed with following error: %v\neos: %s output: %s", program, err, program, out)
	}

	return nil
}
