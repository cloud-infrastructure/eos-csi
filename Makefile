.PHONY: all eosplugin

NAME=csi-eosplugin
IMAGE_VERSION=v0.1.0

all: eosplugin

test:
	go test gitlab.cern.ch/cloud-infrastructure/eos-csi/pkg/... -cover
	go vet gitlab.cern.ch/cloud-infrastructure/eos-csi/pkg/...

eosplugin:
	CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o  _output/$(NAME) ./eos

image: eosplugin 
	cp _output/$(NAME) deploy/docker
	docker build -t $(NAME):$(IMAGE_VERSION) deploy/docker

clean:
	go clean -r -x
