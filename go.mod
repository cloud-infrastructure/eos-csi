module gitlab.cern.ch/cloud-infrastructure/eos-csi

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/pborman/uuid v1.2.0
	google.golang.org/grpc v1.16.0
	k8s.io/apimachinery v0.0.0-20181130031032-af2f90f9922d // indirect
	k8s.io/kubernetes v1.12.3
	k8s.io/utils v0.0.0-20181115163542-0d26856f57b3 // indirect
)
