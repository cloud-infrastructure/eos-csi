package main

import (
	"flag"
	"os"
	"path"

	"github.com/golang/glog"
	"gitlab.cern.ch/cloud-infrastructure/eos-csi/pkg/eos"
)

func init() {
	flag.Set("logtostderr", "true")
}

var (
	endpoint   = flag.String("endpoint", "unix://tmp/csi.sock", "CSI endpoint")
	driverName = flag.String("drivername", "csi-eos", "name of the driver")
	nodeId     = flag.String("nodeid", "", "node id")
)

func main() {
	flag.Parse()

	if err := os.MkdirAll(path.Join(eos.PluginFolder, "controller"), 0755); err != nil {
		glog.Errorf("failed to create persistent storage for controller: %v", err)
		os.Exit(1)
	}

	if err := os.MkdirAll(path.Join(eos.PluginFolder, "node"), 0755); err != nil {
		glog.Errorf("failed to create persistent storage for node: %v", err)
		os.Exit(1)
	}

	driver := eos.NewEOSDriver()
	driver.Run(*driverName, *nodeId, *endpoint)

	os.Exit(0)
}
