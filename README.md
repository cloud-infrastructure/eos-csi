# EOS CSI driver

csi-eos provides mounting of EOS repositories in CSI-enabled container orchestrators.

## Compiling

The CSI EOS driver can be compiled in a form of a binary file or an image. When compiled as a binary, the resulting file gets stored in `_output` directory. When compiled as an image, it gets stored in local Docker image store.

Building a binary file: `$ make eosplugin`
Building a Docker image: `$ make image`

## Deployment

## StorageClass parameters


### EOS configuration

