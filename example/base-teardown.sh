#!/bin/bash

objects=(csi-eosplugin-attacher csi-eosplugin-provisioner csi-eosplugin csi-eosplugin-attacher-rbac csi-eosplugin-provisioner-rbac csi-eosplugin-rbac)

for obj in "${objects[@]}"; do
	kubectl delete -f "./$obj.yaml"
done
