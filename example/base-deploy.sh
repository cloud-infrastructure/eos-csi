#!/bin/bash

objects=(csi-eosplugin-attacher-rbac csi-eosplugin-provisioner-rbac csi-eosplugin-rbac csi-eosplugin-attacher csi-eosplugin-provisioner csi-eosplugin)

for obj in "${objects[@]}"; do
	kubectl create -f "./$obj.yaml"
done
